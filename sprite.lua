local Class = require 'hump.class'

local Sprite = Class{}

function Sprite:init(x, y, angle)
  self.x = x or 0
  self.y = y or 0
  self.angle = angle or 0
end

function Sprite:update(dt)

end

function Sprite:draw()
  love.graphics.translate(self.x, self.y)
  love.graphics.rotate(self.angle)
end

return Sprite
