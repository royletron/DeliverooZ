local Class = require 'hump.class'

local Layer = Class{}

function Layer:init()
  self.entities = {}
end

function Layer:update(dt)
  for _, v in ipairs(self.entities) do
    v:update(dt)
  end
end

function Layer:draw()
  for _, v in ipairs(self.entities) do
    v:draw()
  end
end

function Layer:add(addable)
  table.insert(self.entities, addable)
end

function Layer:remove(removable)
  for i, v in ipairs(self.entities) do
    if v == removable then
      table.remove(self.entities, i)
      break
    end
  end
  print('Unable to find entity provided to remove')
end

return Layer
