local Class = require 'hump.class'
local Layer = require 'layer'
local Spot = require 'spot'
local flux = require 'flux.flux'

local Pedals = Class{__includes = Layer}

function Pedals:init()
  Layer.init(self)
  self.group = flux.group()
  self.left = Spot(80, 600)
  self.right = Spot(200, 600)
  self:add(self.left)
  self:add(self.right)
  self.pace = 1
  self.l = self:tween(self.left)
  self.r = self:tween(self.right, true)
end

function Pedals:tween(pedal, delay)
  local e = 'quad' .. (pedal.position == 0 and 'in' or 'out')
  local t = pedal.position == 0 and 50 or 0
  return self.group:to(pedal, 2, { position = t }):delay(delay and 2 or 0):ease(e):oncomplete(function() self:tween(pedal) end)
end

function Pedals:keyreleased(key)
  if (key == 'z') then
    local power = (self.left.position - 45) / 100
    print(power)
    self.pace = self.pace + power
  end
  if (key == 'x') then
    local power = (self.right.position - 45) / 100
    print(power)
    self.pace = self.pace + power
  end
  if self.pace > 2 then self.pace = 2
  elseif self.pace < 0.5 then self.pace = 0.5
  end
end

function Pedals:update(dt)
  self.group:update(dt * self.pace)
  Layer.update(self, dt)
end

function Pedals:draw()
  Layer.draw(self)
end

return Pedals
