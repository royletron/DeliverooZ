local Class = require 'hump.class'
local Sprite = require 'sprite'

local Spot = Class{__includes = Sprite}

local MAX_TIME = 1.5

function Spot:init(x, y)
  Sprite.init(self, x, y)
  self.position = 0
end

function Spot:draw()
  love.graphics.setColor(255, 255, 255)
  love.graphics.circle('fill', self.x, self.y, 50, 100)

  love.graphics.setColor(255, 0, 0)
  love.graphics.setLineWidth(3)
  -- print(self.position)
  love.graphics.circle('line', self.x, self.y, self.position, 100)
end

return Spot
