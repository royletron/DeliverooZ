local moonshine = require 'moonshine'
local Layer = require 'layer'
local Pedals = require 'pedals'
local Bike = require 'bike'
local game = {}
local pedals = Pedals()
local bike = Bike(100, 100)
local effect
local entities

function game:init()
  effect = moonshine(moonshine.effects.crt)
    .chain(moonshine.effects.filmgrain)
    -- .chain(moonshine.effects.pixelate)
    .chain(moonshine.effects.scanlines)
    .chain(moonshine.effects.vignette)
  effect.filmgrain.size = 2
  entities = Layer()
  entities:add(pedals)
  entities:add(bike)
end

function game:update(dt)
  entities:update(dt)
end

function game:keyreleased(key)
  pedals:keyreleased(key)
end

function game:draw()
  effect(function()
    entities:draw()
  end)
  love.graphics.translate(0, 0)
  love.graphics.rotate(0)
  love.graphics.print('Pace '..pedals.pace, 50, 20)
end

return game
