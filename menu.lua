Gamestate = require 'hump.gamestate'
game = require 'game'

local menu = {}

function menu:draw()
  love.graphics.print("Press Enter to continue", 20, 10)
end

function menu:keyreleased(key, code)
  if key == 'return' then
    Gamestate.switch(game)
  end
end

return menu
