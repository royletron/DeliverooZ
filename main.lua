local Gamestate = require 'hump.gamestate'
local menu = require 'menu'
local game = require 'game'

function love.load()
  love.window.setMode(1024, 768)
  Gamestate.registerEvents()
  Gamestate.switch(menu)
end

