local Class = require 'hump.class'
local Sprite = require 'sprite'

local Bike = Class{__includes = Sprite}

function Bike:init(x, y)
  Sprite.init(self, x, y)
  self.width = 10
  self.height = 50
end

function Bike:draw()
  Sprite.draw(self)
  love.graphics.setColor(0, 255, 0)
  love.graphics.rectangle('fill', -self.width/2, -self.height/2, self.width, self.height)
end

return Bike
